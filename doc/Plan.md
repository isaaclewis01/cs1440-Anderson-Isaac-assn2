Software Development Plan:

    1. Requirements:
        This program takes in the name of a directory, and uses a python library
        to find and return statistics from the U.S. Bureau of Labor Statistics, 2018.
        This report contains two sections: a summary across all industries and a summary
        across the software publishing industry.

    2. Design:
        This program takes in a directory as user input instead of a file. With this, the file name must be hard-coded
        into the program. With that, the program reads in the file.
        Input:
            User enters a command to run main.py and the directory name. Then the program will read in the hard-coded file,
            pull information from there, and print it out. All input should be strings and spelled correctly.
        Processing:
            When main.py is called, the input chooses which directory to access files from. From there, the program searches
            through the file and pulls out what is needed.
        Output:
            The output should be a two-part message. One part being a summary across all industries, and the other across
            the software publishing industry. The output should directly resemble Erik's given output examples.

    3. Implementation:
        This program is entirely located in the src folder, each module being contained in there.
        User is required input through the command line.
        The only argument is a directory name.

    4. Verification:
        Test One:
            Did not give an argument...
                Printed Usage message, success.
        Test Two:
            Did output match Erik's when user enters "UT_all"...
                Success
