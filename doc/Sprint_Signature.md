| Date | Events |
| ---- | ------ |
| 2/10/20 | Cloned Repository |
| 2/11/20 - 2/17/20 | Had no time to work |
| 2/18/20 | Began project, created Sprint_Signature and Plan.md |
| 2/19/20 | Began coding, created dictionary and 2018.annual.singlefile.csv for each directory |
| 2/20/20 | Finished program, maintained |
| 2/21/20 | Fixed maxWageArea |