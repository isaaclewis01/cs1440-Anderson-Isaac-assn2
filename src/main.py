import time
import sys
from Report import Report

rpt = Report()

# if sys.argv[1] is not given, print a usage message and exit
if len(sys.argv) <= 1:
    print("Usage: src/main.py DATA_DIRECTORY", file=sys.stderr)
    exit()

#print("Reading the databases...", file=sys.stderr)
before = time.time()

# if opening the file 'sys.argv[1]/area_titles.csv' fails, let your program crash here
# Convert the file 'sys.argv[1]/area_titles.csv' into a dictionary
dictionary = {}
f = open(sys.argv[1] + '/area_titles.csv')
for line in f:
    values = line.rstrip("\n").split(sep=',', maxsplit=1)
    firstColumn = values[0].strip('\"').rstrip('\"')
    if firstColumn.isdigit() and not (firstColumn[-3:] == '000') and not (firstColumn[:2] == 'US') and not (firstColumn[0] == 'C'):
        dictionary[values[0].strip("\"").rstrip("\"")] = values[1].strip("\"").rstrip("\"")
'''for state in dictionary:
    print(f"{state} | {dictionary[state]}")'''

# if opening the file 'sys.argv[1]/2018.annual.singlefile.csv' fails, let your program crash here
# Collect information from 'sys.argv[1]/2018.annual.singlefile.csv', place into the Report object rpt
# Open giant 2018.annual.singlefile.csv file...
f = open(sys.argv[1] + '/2018.annual.singlefile.csv')
# Variables to determine if it is all industries or software publishing industry
industryCode = 0
ownCode = 0
# Variables to keep track of data
# 'All' variables
allFipsCounter = 0

allGrossAnnualWages = 0
allMaxWage = 0
allMaxWageArea = ''

allTotalEstabs = 0
allMaxEstabs = 0
allMaxEstabsArea = ''

allGrossAnnualEmploy = 0
allMaxEmploy = 0
allMaxEmployArea = ''

# 'Soft' variables
softFipsCounter = 0

softGrossAnnualWages = 0
softMaxWage = 0
softMaxWageArea = ''

softTotalEstabs = 0
softMaxEstabs = 0
softMaxEstabsArea = ''

softGrossAnnualEmploy = 0
softMaxEmploy = 0
softMaxEmployArea = ''

for line in f:
    data = line.rstrip().split(sep=',')
    data0 = data[0].rstrip("\"").strip("\"")
    if dictionary.get(data0) is not None:
        # Find data for all industries
        if data[2].rstrip("\"").strip("\"") == '10' and data[1].rstrip("\"").strip("\"") == '0':
            # To find number of FIPS
            allFipsCounter += 1
            # To find wage info
            allGrossAnnualWages += int(data[10])
            currentWageMax = int(data[10])
            if currentWageMax > allMaxWage:
                allMaxWage = currentWageMax
            if int(data[10]) == allMaxWage:
                allMaxWageArea = dictionary.get(data0)
            # To find establishment info
            allTotalEstabs += int(data[8])
            currentEstabMax = int(data[8])
            if currentEstabMax > allMaxEstabs:
                allMaxEstabs = currentEstabMax
            if int(data[8]) == allMaxEstabs:
                allMaxEstabsArea = dictionary.get(data0)
            # To find gross annual employment info
            allGrossAnnualEmploy += int(data[9])
            currentEmployMax = int(data[9])
            if currentEmployMax > allMaxEmploy:
                allMaxEmploy = currentEmployMax
            if int(data[9]) == allMaxEmploy:
                allMaxEmployArea = dictionary.get(data0)

        # Find data for software publishing industry
        if data[2].rstrip("\"").strip("\"") == '5112' and data[1].rstrip("\"").strip("\"") == '5':
            # To find number of FIPS
            softFipsCounter += 1
            # To find wage info
            softGrossAnnualWages += int(data[10])
            currentWageMax = int(data[10])
            if currentWageMax > softMaxWage:
                softMaxWage = currentWageMax
            if int(data[10]) == softMaxWage:
                softMaxWageArea = dictionary.get(data0)
            # To find establishment info
            softTotalEstabs += int(data[8])
            currentEstabMax = int(data[8])
            if currentEstabMax > softMaxEstabs:
                softMaxEstabs = currentEstabMax
            if int(data[8]) == softMaxEstabs:
                softMaxEstabsArea = dictionary.get(data0)
            # To find gross annual employment info
            softGrossAnnualEmploy += int(data[9])
            currentEmployMax = int(data[9])
            if currentEmployMax > softMaxEmploy:
                softMaxEmploy = currentEmployMax
            if int(data[9]) == softMaxEmploy:
                softMaxEmployArea = dictionary.get(data0)

after = time.time()
#print(f"Done in {after - before:.3f} seconds!", file=sys.stderr)



# Fill in the report for all industries
rpt.all.num_areas           = allFipsCounter

rpt.all.gross_annual_wages  = allGrossAnnualWages
rpt.all.max_annual_wage     = (allMaxWageArea, allMaxWage)

rpt.all.total_estab         = allTotalEstabs
rpt.all.max_estab           = (allMaxEstabsArea, allMaxEstabs)

rpt.all.total_empl          = allGrossAnnualEmploy
rpt.all.max_empl            = (allMaxEmployArea, allMaxEmploy)


# Fill in the report for the software publishing industry
rpt.soft.num_areas          = softFipsCounter

rpt.soft.gross_annual_wages = softGrossAnnualWages
rpt.soft.max_annual_wage    = (softMaxWageArea, softMaxWage)

rpt.soft.total_estab        = softTotalEstabs
rpt.soft.max_estab          = (softMaxEstabsArea, softMaxEstabs)

rpt.soft.total_empl         = softGrossAnnualEmploy
rpt.soft.max_empl           = (softMaxEmployArea, softMaxEmploy)

# Print the completed report
print(rpt)
