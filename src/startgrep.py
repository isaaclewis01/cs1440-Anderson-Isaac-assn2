import sys

def startgrep(pat, args):
    """print lines of files beginning with a pattern"""
    pattern = pat
    for fname in args:
        f = open(fname)
        for line in f:
            if line.startswith(pattern):
                print(line, end='')
        f.close()

startgrep(pat=sys.argv[1], args=sys.argv[2:])